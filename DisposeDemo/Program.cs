﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Henn.Demod.DisposableDemo
{
    static class Program
    {

        //static bool KasLeidub<T> (this IEnumerable<T> mass, T sellline)
        //{
        //    bool vastus = false;
        //    foreach (var x in mass) if(x == sellline) { vastus = true; break; }
        //    return vastus;
        //}

        static void Main(string[] args)
        {

 

            for (int i = 0; i < 1000; i++)
            {
                using (var x = new Suur())
                {
                    Console.WriteLine(x);
                }
            }
        }
    }

    class Suur : IDisposable
    {
        static int Mitu = 0;
        static int Surnuid = 0;
        byte[] sisu = new byte[200_000_000];
        int Mitmes = Mitu++;
        ~Suur() 
        {
            Console.WriteLine($"Suur nr {Mitmes} ({Mitu}-st) tapeti maha");
            Surnuid++;
            Dispose(false);
        }

        public void Vabasta()
        {
            Console.WriteLine($"{Mitmes} lasi lahti");
            Surnuid++;
        }

        public override string ToString()
        {
            return $"Suur nr {Mitmes} (kokku: {Mitu} neist elus {Mitu-Surnuid})";
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.
                Vabasta();
                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Suur() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
             GC.SuppressFinalize(this);
        }
        #endregion
    }
}
